### tlcrecord

Simple screen recording tool

#### install

No need to install anything, all commands will run from extracted directory

#### how to use

From the extracted/cloned folder, execute the following commands

##### start recording

$ tlcrecord

##### start camera

press c then press enter key

##### stop camera

press c again then press enter key

#### stop

press q then press enter key

### tlcrecord-shorts

Simple tool to create youtube shorts

#### install

No need to install anything, all commands will run from extracted directory

#### how to use

From the extracted/cloned folder, execute the commands

##### example commandline

`
$ tlcrecord-shorts shorts0.mp4 shorts0.txt
`
